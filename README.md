A demo application built on top of Framework7. This app will list upcoming and current movies from theaters and give you detailed information. It is based on the Official TMDb API.
This app is multi-platform. You can use it throw your web browser on your smartphone, tablet or desktop because it's responsive.
Note that With PhoneGap you can easily convert it to native iOS & Android app.


# Screenshot
<img src="https://2.bp.blogspot.com/-QGXiTghIvFE/WKQwNmMG62I/AAAAAAAABxc/GfDwOvBIEgIP6xzFakzuZKk7m7dbwUCRwCLcB/s1600/dfdssssd1.jpg"/>